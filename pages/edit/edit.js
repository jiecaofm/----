Page({
  data: {
    searchRet: [],
    searchRetHidden: true,
    searchEditHidden: false,
    editHidden: true,
    recommend: '',
    submit: {},
    title: '',
  },
  bindblur: function (e) {
    console.log('开始搜索 ' + 'https://wxapp.jiecao.fm/movie/request?url=https://api.douban.com/v2/movie/search?q=' + e.detail.value)
    wx.showToast({
      title: '加载中',
      icon: 'loading'
    })
    var thiss = this;
    wx.request({
      url: encodeURI('https://wxapp.jiecao.fm/movie/request?url=https://api.douban.com/v2/movie/search?q=' + e.detail.value),
      method: 'GET', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
      header: {
        "Content-Type": "json"
      }, // 设置请求的 header
      success: function (res) {
        // success
        console.log(res)
        thiss.setData({
          searchRet: res.data.subjects,
          searchRetHidden: false,
          searchEditHidden: false,
          editHidden: true,
        })
      },
      fail: function (e) {
        // fail
        console.log(e)
      },
      complete: function () {
        // complete
        wx.hideToast();
      }
    })
  },
  searchRetItemTap: function (event) {
    wx.showToast({
      title: '加载中',
      icon: 'loading'
    })
    //点击item隐藏列表，显示电影数据
    var thiss = this;
    console.log(event)
    wx.request({
      url: encodeURI('https://wxapp.jiecao.fm/movie/request?url=https://api.douban.com/v2/movie/subject/' + event.target.dataset.alphaBeta),
      data: {},
      method: 'GET', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
      header: {
        "Content-Type": "json"
      },
      success: function (res) {
        console.log('get subject succ')
        console.log(res)
        // success
        var tmp = {}
        tmp.id = res.data.id
        tmp.title = res.data.title
        tmp.images = res.data.images.large
        tmp.genres = res.data.genres//类型
        tmp.aka = res.data.aka //又名
        tmp.year = res.data.year//年份
        tmp.summary = res.data.summary //简介
        tmp.directors = res.data.directors//导演
        tmp.casts = res.data.casts//演员
        thiss.setData({
          searchRetHidden: true,
          searchEditHidden: true,
          editHidden: false,
          submit: tmp,
          title: tmp.title,
        })
        console.log(thiss.data.submit)
      },
      fail: function () {
        // fail
      },
      complete: function () {
        // complete
        wx.hideToast()
      }
    })
  },
  submit: function (event) {
    var thiss = this
    wx.showToast({
      title: '加载中',
      icon: 'loading'
    })
    var send = this.data.submit
    send.recommend = this.data.recommend
    console.log('推荐')
    console.log(send)
    console.log(this.data)
    var url = encodeURI("https://wxapp.jiecao.fm/movie/put")//?name=" + send.title + '&detail=')//+ JSON.stringify(send))
    console.log(url)
    //请求自己的服务器，提交数据
    wx.request({
      url: encodeURI(url),
      method: 'POST', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
      header: {
        // "Content-Type": "application/x-www-form-urlencoded",
        // "Accept":"application/json"
      }, // 设置请求的 header
      // data: {
      // 'name': send.title,
      // 'detail': JSON.stringify(send)
      // },
      data: send.title + '##########' + JSON.stringify(send),
      success: function (ress) {
        // success
        console.log('推荐成功')
        console.log(ress)
        wx.showModal({
          title: '提示',
          content: '推荐成功',
          success: function (res) {
            if (res.confirm) {
              console.log('用户点击确定' + ress.data)
              //预览推荐电影
              if (res.data < 1) return
              wx.redirectTo({
                url: '../detail/detail?movie_id=' + ress.data,
                success: function (res) {
                  // success
                },
                fail: function () {
                  // fail
                },
                complete: function () {
                  // complete
                }
              })
            }
          }
        })
      },
      fail: function (res) {
        // fail
        console.log('fail')
        console.log(res)
      },
      complete: function () {
        // complete
        wx.hideToast()
      }
    })
  },
  recommendChange: function (event) {
    console.log(event)
    this.setData({
      recommend: event.detail.value
    })
  },
})