// pages/start/start.js
Page({
  data: {

  },
  toDetail: function () {
    var value = wx.getStorageSync(this.getToday());
    if (value < 6) {
      wx.navigateTo({
        url: '../detail/detail'
      })
    } else {
      wx.showToast({
        title: '请明天再来吧~',
        duration: 3000
      })
    }
  },
  toRecommand: function () {
    wx.navigateTo({
      url: '../edit/edit'
    })
  },
  getToday: function () {
    var date = new Date;
    var year = date.getFullYear()
    var month = date.getMonth()+1024
    var day = date.getDate()
    return "" + year + month + day
  }
})