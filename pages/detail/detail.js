// pages/detail/detail.js
Page({
  data: {
    is_hidden: true,
    imagesrc: null,
    movie_year: null,
    movie_title: null,
    movie_genres: null,
    casts: null,
    summary: null,
    isloaded: false,
    isLoading: true,
    genres: null,
    aka: null,
    summary: null,
    summary_css: "summary-content-part",
    is_summary_all: false,
    change_css_text: "展开",
  },
  onLoad: function (options) {
    wx.showToast({
      title: '加载中',
      icon: 'loading'
    })
    var page = this;

    //页面初始化 options为页面跳转所带来的参数
    var link;
    var id = options.movie_id;
    if (id == undefined || id == '') {
      //获取今日已经读取过的电影
      var ids = '0';
      var res = wx.getStorageSync(page.getToday() + 'IDS');
      if (res != '' && res != undefined) {
        ids = res
      }
      link = 'https://wxapp.jiecao.fm/movie/random_get?ids=' + ids
    } else {
      link = 'https://wxapp.jiecao.fm/movie/get?id=' + id
    }
    console.log('link: ' + link)
    wx.request({
      url: link,
      data: {},
      method: 'GET',
      header: { "Content-Type": "json" },
      success: function (res) {
        console.log(res.data);
        if (res.data.status.code == 0) {
          var json = JSON.parse(res.data.jcdata[0].detail);
          page.setData({
            imagesrc: json.images,
            movie_year: json.year,
            movie_title: json.title,
            movie_genres: json.genres,
            casts: json.casts,
            summary: json.summary,
            genres: json.genres,
            aka: json.aka,
            reason: json.recommend,
            is_hidden: false
          })
          wx.hideToast();
          //当不是用户推荐后转到此页面的前提时,记录此次查看
          if (options.movie_id == undefined || options.movie_id == '') {
            wx.setStorageSync(page.getToday() + 'IDS', ids + ',' + res.data.jcdata[0].id);
            //记录今日成功读取次数
            var today = page.getToday();
            var value = wx.getStorageSync(today);
            if (value == '') {
              value = 1;
            }
            console.log(value);
            wx.setStorageSync(today, Number(value) + 1);
          }
        } else {
          wx.hideToast();
          wx.showToast({
            title: '网络错误',
            duration: 2000
          })
        }
      },
      fail: function () {
        wx.hideToast();
        wx.showToast({
          title: '网络错误',
          duration: 2000
        })
      },
      complete: function () {

      }
    })

  },
  changCSS: function () {
    var page = this;
    if (page.data.is_summary_all) {
      page.setData({
        summary_css: "summary-content-part",
        is_summary_all: false,
        change_css_text: "展开"
      })
    } else {
      page.setData({
        summary_css: "summary-content-all",
        is_summary_all: true,
        change_css_text: "收起"
      })
    }
  },
  getToday: function () {
    var date = new Date;
    var year = date.getFullYear()
    var month = date.getMonth() + 1024
    var day = date.getDate()
    return "" + year + month + day
  }
})